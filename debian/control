Source: r-cran-epibasix
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-epibasix
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-epibasix.git
Homepage: https://cran.r-project.org/package=epibasix
Rules-Requires-Root: no

Package: r-cran-epibasix
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R Elementary Epidemiological Functions
 Elementary Epidemiological Functions for a Graduate Epidemiology /
 Biostatistics Course.
 .
 This package contains elementary tools for analysis of common epidemiological
 problems, ranging from sample size estimation, through 2x2 contingency table
 analysis and basic measures of agreement (kappa, sensitivity/specificity).
 Appropriate print and summary statements are also written to facilitate
 interpretation wherever possible. This package is a work in progress, so
 any comments or suggestions would be appreciated. Source code is commented
 throughout to facilitate modification. The target audience includes graduate
 students in various epi/biostatistics courses.
 .
 Epibasix was developed in Canada.
